/*
 * klimper.h -- definitions for the char module
 *
 * Copyright (C) 2001 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 * $Id: klimper.h,v 1.15 2004/11/04 17:51:18 rubini Exp $
 */

#ifndef _KLIMPER_H_
#define _KLIMPER_H_

#undef PDEBUG             /* undef it, just in case */
#ifdef KLIMPER_DEBUG
#ifdef __KERNEL__
/* This one if debugging is on, and kernel space */
#define PDEBUG(fmt, args...) printk( KERN_DEBUG "klimper: " fmt, ## args)
#else
/* This one for user space */
#define PDEBUG(fmt, args...) fprintf(stderr, fmt, ## args)
#endif
#else
#define PDEBUG(fmt, args...) /* not debugging: nothing */
#endif /* KLIMPER_DEBUG */

#ifndef KLIMPER_MAJOR
#define KLIMPER_MAJOR   0
#endif /* KLIMPER_MAJOR */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/types.h>	// size_t
#include <linux/kdev_t.h>	// dev_t
#include <linux/fs.h>		// vieles...
#include <linux/fcntl.h>	// file operations
#include <linux/cdev.h>
#include <linux/kernel.h>	// printk()
#include <linux/errno.h>	// Error Numbers
#include <linux/slab.h>		// kmalloc()
#include <linux/jiffies.h>	// timers
#include <asm/uaccess.h>	// copy_from_user() .. to_user()
#include <linux/string.h>

#define SUCCESS		0
#define VOID_RET_VAL	-1 // void or return value (if fail)
#define FIRST_MINOR 	0 // minor number

struct klimper_dev {
    u64 t0;
    u64 t1;
    int time_delta; // last time measurement
    struct semaphore wr_lock; // Nur ein Prozess darf die Datei zum Schreiben geöffnet haben
    struct semaphore rd_lock; // Nur ein Prozess darf die Datei zum Lesen geöffnet haben
    struct semaphore mutex; // Nur ein Prozess darf Lesen oder Schreiben
    struct cdev cdev; // char device structure
};

/*
 * Prototypes for shared functions
 */

static int klimper_init(void);
static void klimper_cleanup(void);
static void klimper_setup_cdev(struct klimper_dev *klimper_device);

int klimper_open(struct inode *inode, struct file *filp);
int klimper_release(struct inode *inode, struct file *filp);
ssize_t klimper_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
ssize_t klimper_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos);

struct file_operations klimper_fops = {
    .owner = THIS_MODULE,
    .read = klimper_read,
    .write = klimper_write,
    .open = klimper_open,
    .release = klimper_release
};

#endif /* _KLIMPER_H_ */
