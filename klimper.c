/*
 * pipe.c -- fifo driver for klimper
 *
 * Copyright (C) 2001 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 */

#include "klimper.h"		/* local definitions */

MODULE_AUTHOR("Johannes Berger, Edgar Toll");
MODULE_DESCRIPTION("Gibt die Zeit zwischen zwei Newlines in ms zurück");
MODULE_LICENSE("Dual BSD/GPL");

int klimper_major = KLIMPER_MAJOR;
int ret_val = VOID_RET_VAL;
struct klimper_dev* klimper_device = NULL;

module_param(klimper_major, int, 0);
module_param(ret_val, int, 0);

/*
 * Open and close
 */
int klimper_open(struct inode *inode, struct file *filp) {
    klimper_device = container_of(inode->i_cdev, struct klimper_dev, cdev);
    filp->private_data = klimper_device;

    PDEBUG("open()\n");
    if ((filp->f_mode & FMODE_WRITE) == FMODE_WRITE) {
        if (down_trylock(&klimper_device->wr_lock) != 0) {
            PDEBUG("open() -- Device being already written to\n");
            return -EBUSY;
        }
    } else {
        if (down_trylock(&klimper_device->rd_lock) != 0) {
            PDEBUG("open() -- Device being already read from\n");
            return -EBUSY;
        }
    }

    return SUCCESS;
}

int klimper_release(struct inode *inode, struct file *filp) {

    PDEBUG("release()\n");
    if ((filp->f_mode & FMODE_WRITE) == FMODE_WRITE) {
        up(&klimper_device->wr_lock);
    } else {
        up(&klimper_device->rd_lock);
    }
    return SUCCESS;
}

/*
 * Data management: read and write
 */
ssize_t klimper_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos) {
    ssize_t num_cp_items = 0;
    char time_val[80];
    int slen = 0;

    PDEBUG("read()\n");
    if (down_interruptible(&klimper_device->mutex)) {
        return -ERESTARTSYS;
    }

    snprintf(time_val, sizeof (time_val), "Zeit zwischen den letzten beiden Newlines: %d ms\n", klimper_device->time_delta);

    slen = strlen(time_val);

    if (*f_pos > slen + 1) { // EOF
        up(&klimper_device->mutex);
        return 0;
    }

    PDEBUG("read()  -- Count: %zd\n", count);
    PDEBUG("read()  -- F_Pos: %lld\n", *f_pos);
    PDEBUG("read()  -- String Length: %d\n", slen);
    PDEBUG("read()  -- Time: %s\n", time_val);

    while (num_cp_items < count && num_cp_items <= slen) {
        if (copy_to_user(buf, &time_val[num_cp_items], 1)) {
            up(&klimper_device->mutex);
            return -EFAULT;
        }
        buf += sizeof (char);
        num_cp_items++;
    }

    *f_pos += num_cp_items;

    up(&klimper_device->mutex);
    return num_cp_items;
}

ssize_t klimper_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos) {
    ssize_t num_cp_items = 0;
    char c = ' ';
    int delta = 0;

    PDEBUG("write()\n");
    if (down_interruptible(&klimper_device->mutex)) {
        return -ERESTARTSYS;
    }

    if (klimper_device->time_delta == VOID_RET_VAL) {
        klimper_device->t0 = get_jiffies_64();
    }

    while (num_cp_items < count) {
        if (copy_from_user(&c, buf, 1)) {
            up(&klimper_device->mutex);
            return -EFAULT;
        }

        if (c == '\n') {
            klimper_device->t1 = get_jiffies_64();
            PDEBUG("write()  -- t0:%llu, t1:%llu\n", klimper_device->t0, klimper_device->t1);
            delta = klimper_device->t1 - klimper_device->t0;
            klimper_device->t0 = klimper_device->t1;
        }
        num_cp_items++;
        buf += sizeof (char);
    }

    PDEBUG("write()  -- last time:%d\n", delta);
    klimper_device->time_delta = delta;
    *f_pos += num_cp_items;

    up(&klimper_device->mutex);

    return num_cp_items;
}

/*
 * Set up a cdev entry.
 */
static void klimper_setup_cdev(struct klimper_dev *klimper_device) {
    int err = 0;
    int devno = MKDEV(klimper_major, FIRST_MINOR);

    cdev_init(&klimper_device->cdev, &klimper_fops);
    klimper_device->cdev.owner = THIS_MODULE;
    klimper_device->cdev.ops = &klimper_fops;
    err = cdev_add(&klimper_device->cdev, devno, 1);

    /* Fail gracefully if need be */
    if (err) {
        PDEBUG("setup_cdev(): Error setting up cdev (#%d)\n", err);
    }
}

/*
 * Initialize the klimper devs; return how many we did.
 */
static int __init klimper_init(void) {
    int resu = SUCCESS;
    dev_t dev;

    PDEBUG("init()\n");
    if (klimper_major) { // Major preset
        dev = MKDEV(klimper_major, FIRST_MINOR);
        resu = register_chrdev_region(dev, 1, "klimper");
    } else { // get dynamic major number
        resu = alloc_chrdev_region(&dev, FIRST_MINOR, 1, "klimper");
        klimper_major = MAJOR(dev);
    }

    if (resu != SUCCESS) {
        printk(KERN_WARNING "init() --Error %d. Can't get major (%d)\n", resu, klimper_major);
        return resu;
    } else {
        PDEBUG("init() -- My Major number is %d\n", klimper_major);
    }

    klimper_device = kmalloc(sizeof (struct klimper_dev), GFP_KERNEL);

    if (!klimper_device) {
        resu = -ENOMEM;
        return resu;
    }

    memset(klimper_device, 0, sizeof (struct klimper_dev));

    klimper_device->time_delta = ret_val;

    sema_init(&klimper_device->wr_lock, 1);
    sema_init(&klimper_device->rd_lock, 1);
    sema_init(&klimper_device->mutex, 1);

    klimper_setup_cdev(klimper_device);

    PDEBUG("init() -- Initialization done, let the disaster begin!\n");

    return SUCCESS;

fail:
    klimper_cleanup();
    return resu;
}

/*
 * This is called by cleanup_module or on failure.
 * It is required to never fail, even if nothing was initialized first
 */
static void klimper_cleanup(void) {
    dev_t dev = MKDEV(klimper_major, FIRST_MINOR);

    PDEBUG("exit()\n");

    if (klimper_device != NULL) {
        cdev_del(&klimper_device->cdev);
        kfree(klimper_device);
        klimper_device = NULL;
    }

    PDEBUG("exit() -- Unregister device with major number %d\n", klimper_major);
    unregister_chrdev_region(dev, 1);
    PDEBUG("exit() -- Goodbye, cruel world!\n");
}

module_init(klimper_init);
module_exit(klimper_cleanup);
