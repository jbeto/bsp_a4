#!/bin/sh
module="klimper"
device="klimper"

# Remove kernel module
sudo /sbin/rmmod $module $*

# Remove Device-Node
sudo rm -f /dev/${device}
