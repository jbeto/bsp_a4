#!/bin/sh
module="klimper"
device="klimper"
mode="666"

# Modul kompilieren
make || exit 1

# Kernelmodul laden
sudo /sbin/insmod ./$module.ko $* || exit 1

# Remove compilation files
make clean

# Major-Nr. holen
major_num=$(awk "\$2==\"$module\" {print \$1}" /proc/devices)
echo $major_num

# Alte Device-Nodes entfernen
sudo rm -f /dev/${device}

# Neue Device-Nodes erstellen
sudo mknod /dev/${device} c $major_num 0

#Gruppen und Zugriffsrechte erteilen
group="staff"
grep -q "^staff:" /etc/group || group="wheel"
sudo chgrp $group /dev/${device}
sudo chmod $mode /dev/${device}
